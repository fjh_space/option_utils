from dash import Dash, dcc, html


app = Dash(__name__)


app.layout = html.Div([])


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8191, debug=True)
