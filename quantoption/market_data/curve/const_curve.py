from math import exp
from quantoption.market_data.curve.curve import *


class ConstantCurve(ABC, BaseCurve):
    def __init__(self,
                 valuation_date: datetime,
                 rate: float
                 ):
        super(ConstantCurve, self).__init__(valuation_date)
        self.__rate = rate

    def rate(self, tau):
        return self.__rate

    def discount(self, tau):
        return exp(-self.__rate * tau)
