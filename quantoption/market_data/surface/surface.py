from abc import ABC, abstractmethod


class BaseSurface(ABC):
    @abstractmethod
    def var(self, *args):
        raise NotImplemented

    @abstractmethod
    def vol(self, *args):
        raise NotImplemented


