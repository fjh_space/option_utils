from math import exp, log, sqrt
from scipy.special import ndtr
from quantoption.utils.enums import OptionTypeEnum
from quantoption.utils.funcs import UtilFunctions


class VanillaEuropean(object):
    @staticmethod
    def __d1(spot, strike, vol, r, q, tau) -> float:
        return (log(spot / strike) + tau * (r - q + .5 * vol ** 2)) / (tau * sqrt(vol))

    @staticmethod
    def __d2(spot, strike, vol, r, q, tau) -> float:
        return VanillaEuropean.__d1(spot, strike, vol, r, q, tau) - tau * sqrt(vol)

    @staticmethod
    def price(spot, strike, vol, r, q, tau, option_type: OptionTypeEnum) -> float:
        if UtilFunctions.small_enough(tau):
            return max(spot - strike, 0) if option_type == OptionTypeEnum.Call else max(strike - spot, 0)
        d1 = VanillaEuropean.__d1(spot, strike, vol, r, q, tau)
        d2 = VanillaEuropean.__d2(spot, strike, vol, r, q, tau)
        if option_type == OptionTypeEnum.Call:
            return spot * exp(-q * tau) * ndtr(d1) - strike * exp(-r * tau) * ndtr(d2)
        else:
            return strike * exp(-r * tau) * ndtr(-d2) - spot * exp(-q * tau) * ndtr(-d1)
