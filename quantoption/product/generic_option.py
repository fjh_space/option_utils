from abc import ABC
from datetime import datetime


class GenericOption(ABC):
    def __init__(self,
                 underlying_code: str,
                 expiration_date: datetime
                 ):
        self.__underlying_code = underlying_code
        self.__expiration_date = expiration_date

    @property
    def underlying_code(self) -> str:
        return self.__underlying_code

    @property
    def expiration_date(self) -> datetime:
        return self.__expiration_date
