from quantoption.product.generic_option import *
from quantoption.utils.enums import OptionTypeEnum


class EuropeanOption(ABC, GenericOption):
    def __init__(self,
                 underlying_code: str,
                 expiration_date: datetime,
                 strike: float,
                 option_type: OptionTypeEnum,
                 delivery_date: datetime
                 ):
        super(EuropeanOption, self).__init__(underlying_code, expiration_date)
        self.__strike = strike
        self.__option_type = option_type
        self.__delivery_date = delivery_date

    @property
    def strike(self) -> float:
        return self.__strike

    @property
    def option_type(self) -> OptionTypeEnum:
        return self.__option_type

    @property
    def delivery_date(self) -> datetime:
        return self.__delivery_date
