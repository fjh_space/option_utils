from enum import Enum


class OptionTypeEnum(Enum):
    Call = 0
    Put = 1
