class UtilFunctions:
    @staticmethod
    def small_enough(value):
        return True if value < 1. / 365 else False
